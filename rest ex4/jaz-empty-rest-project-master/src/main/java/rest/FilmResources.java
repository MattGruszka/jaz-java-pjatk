package rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Actor;
import domain.Comment;
import domain.Film;
import domain.services.FilmService;

@Path("/film")
public class FilmResources {

	private FilmService db = new FilmService();
	
	//1.wyswietlanie wszystkich filmow -dziala
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Film> getAll() {
		return db.getAll();
	}
	
	//3.dodanie nowego filmu -dziala
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Add(Film film) {
		db.add(film);
		return Response.ok(film.getId()).build();
	}
	
	
	//2.wyswietlenie filmu o podanym id -dziala
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") int id) {
		Response result = Response.status(404).build();
		for (Film film : db.getAll()) {
			if (film != null) {
				if (film.getId() == id)
					result = Response.ok(film).build();
			}
		}
		return result;
	}
	
	//4,aktualizacja info o filmie -dziala
	@PUT
	@Path("/{id}/{title}/{description}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id, @PathParam("title") String title,
			@PathParam("description") String description, Film f) {
		Film result = db.get(id);
		if (result == null)
			return Response.status(404).build();
		f.setId(id);
		f.setTitle(title);
		f.setDescription(description);
		db.update(f);
		return Response.ok().build();
	}
	
	
	//7. usuniecie komentarza
   	@DELETE
    @Path("/{id}/comments/{cid}")
    public Response delete(@PathParam("id") int id, @PathParam("cid") int cid) {
        Film result = db.get(id);
        if (result == null)
            return Response.status(404).build();
		if (result.getComments() != null) {
            for (Comment c : result.getComments()) {
                if (c.getId() == cid) {
                		result.getComments().remove(c);
                    result.setComments(result.getComments());
                    return Response.ok().build();
                }
            }
        }
        return Response.status(404).build();
    }
	
	//5.wyswietlenie komentarzy
	@GET
	@Path("/{id}/comments")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getComments(@PathParam("id") int id) {
		Film result = db.get(id);
		if (result == null)
			return null;
		if (result.getComments() == null)
			result.setComments(new ArrayList<Comment>());
		return result.getComments();
	}
	
	//6. dodanie komentarza
	@POST
	@Path("/{id}/comments")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addComment(@PathParam("id") int id, Comment comment) {
		Film result = db.get(id);
		if (result == null)
			return null;
		if (result.getComments() == null)
			result.setComments(new ArrayList<Comment>());
		result.getComments().add(comment);
		return Response.ok().build();
	}
	
	//8. wystawienie oceny
	@POST
	@Path("/{id}/{mark}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addMark(@PathParam("id") int id, @PathParam("mark") int mark) {
		Film result = db.get(id);
		if (result == null)
			return null;
		if (result.getComments() == null)
			result.setComments(new ArrayList<Comment>());
		result.addMark(mark);
		return Response.ok().build();
	}
	
}