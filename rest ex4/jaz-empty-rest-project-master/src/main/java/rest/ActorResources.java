package rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Actor;
import domain.Film;
import domain.services.ActorService;
import domain.services.FilmService;

@Path("/actor")
public class ActorResources {
	
		private ActorService actorDb = new ActorService();
		private FilmService filmDb = new FilmService();

		
		//1*.dodanie nowego aktora
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		public Response Add(Actor a) {
			actorDb.add(a);
			return Response.ok(a.getId()).build();
		}
		
		//wyswietlanie wszystkich filmow -dziala
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public List<Actor> getAll() {
			return actorDb.getAll();
		}
		
		//2*. przydzielanie filmu danemu aktorowi
				@POST
				@Path("/{idAktor}/{idFilm}")
				@Consumes(MediaType.APPLICATION_JSON)
				public Response addFilmToActor(@PathParam("idAktor") int idAktor, @PathParam("idFilm") int idFilm) {
					Actor result = actorDb.get(idAktor);
					if (result == null)
						return null;
					if (result.getAssignedFilms() == null)
						result.setAssignedFilms(new ArrayList<Film>());
					Film filmResult = filmDb.get(idFilm);
					result.getAssignedFilms().add(filmResult);
					return Response.ok().build();
				}
		
		//3*. wyswietlanie filmow danego aktora
		@GET
		@Path("/{id}/films")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Film> getFilmsByActor(@PathParam("id") int id) {
			Actor result = actorDb.get(id);
			return result.getAssignedFilms();
		}
		
}

