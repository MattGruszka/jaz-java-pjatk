
package domain;

import java.util.List;

public class Film {

	private int id;
	private String title;
	private String description;
	private List<Comment> comments;
	private int avgMark;
	private List<Integer> marks;

	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	public int getAvgMark() {
		return avgMark;
	}

	public void setAvgMark(int avgMark) {
		this.avgMark = avgMark;
	}

	public List<Integer> getMarks() {
		return marks;
	}

	public void setMarks(List<Integer> marks) {
		this.marks = marks;
	}
	

	
	public void addMark(int mark) {
		  int sum = 0;
		marks.add(mark);
		for(int element : marks) {
			sum += element;
			}
		if(marks.size() > 0) {
			avgMark = sum / marks.size();
		}
	}
}

	
