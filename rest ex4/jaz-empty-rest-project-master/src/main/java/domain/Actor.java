package domain;

import java.util.List;

public class Actor {

	private int id;
	private String firstname;
	private String surname;
	private String age;
	private List<Film> assignedFilms;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	public void setAssignedFilms(List<Film> assignedFilms) {
		this.assignedFilms = assignedFilms;
	}

	public List<Film> getAssignedFilms() {
		return assignedFilms;
	}
	
	public void setFilm(Film f) {
		assignedFilms.add(f);
	}
	
}
