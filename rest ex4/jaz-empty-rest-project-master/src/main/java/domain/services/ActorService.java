package domain.services;

import java.util.ArrayList;
import java.util.List;

import domain.Actor;

public class ActorService {
	
		private static List<Actor> actorDb = new ArrayList<Actor>();
		private static int currentId = 1;
		
		public List<Actor> getAll() {
			return actorDb;
		}
		
		public Actor get(int id) {
			for (Actor a : actorDb) {
				if (a.getId() == id)
					return a;
			}
			return null;
		}
		
		
		public void delete(Actor a) {
			actorDb.remove(a);
		}
		
		public void add(Actor a) {
			actorDb.add(a);
		}
		
		
		
	
}
