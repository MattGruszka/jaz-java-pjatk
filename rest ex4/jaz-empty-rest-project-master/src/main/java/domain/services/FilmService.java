package domain.services;

import java.util.ArrayList;
import java.util.List;

import domain.Comment;
import domain.Film;

public class FilmService {
	
	private static List<Film> db = new ArrayList<Film>();
	private static int currentId = 1;
	
	public List<Film> getAll() {
		return db;
	}
	
	public Film get(int id) {
		for (Film f : db) {
			if (f.getId() == id)
				return f;
		}
		return null;
	}
	
	public void update(Film film) {
		for (Film f : db) {
			if (f.getId() == film.getId()) {
				f.setTitle(film.getTitle());
				f.setDescription(film.getDescription());
			}
		}
	}
	
	public void delete(Film f) {
		db.remove(f);
	}
	
	public void add(Film f) {
		db.add(f);
	}
	
//	public void deleteComment(int commentId) {
//		for(Comment element : db.) {
//			  if(element.getId() == commentId) {
//				  comments.remove(element);
//			  }
//			}
//	}
	
	
	
}